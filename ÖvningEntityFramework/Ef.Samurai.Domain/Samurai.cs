﻿using System.Collections.Generic;

namespace EfSamurai.Domain
{
    public class Samurai
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Weapon { get; set; }
        public string Armor { get; set; }
        public HairStyles HairStyle { get; set; }

        public SecretIdentity SecretIdentity { get; set; }

        public ICollection<SamuraiBattle> SamuraiBattles { get; set; }
        public ICollection<Quote> Quotes { get; set; }
    }

    public enum HairStyles
    {
        Chonmage,
        Oicho,
        Western
    }
}
