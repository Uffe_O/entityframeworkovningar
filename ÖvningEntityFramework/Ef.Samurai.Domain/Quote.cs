﻿namespace EfSamurai.Domain
{
    public class Quote
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public QuoteTypes QuoteType { get; set; }

        public int SamuraiId { get; set; }
        public Samurai Samurai { get; set; }
    }

    public enum QuoteTypes
    {
        Lame,
        Cheesy,
        Awesome
    }
}
