﻿namespace EfSamurai.Domain
{
    public class BattleEvent
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Summation { get; set; }

        public int BattleLogId { get; set; }
        public BattleLog BattleLog { get; set; }
    }
}
