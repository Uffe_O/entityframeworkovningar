﻿using System;
using System.Collections.Generic;

namespace EfSamurai.Domain
{
    public class Battle
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Brutal { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        
        public BattleLog BattleLog { get; set; }

        public ICollection<SamuraiBattle> SamuraiBattles { get; set; }
    }
}
