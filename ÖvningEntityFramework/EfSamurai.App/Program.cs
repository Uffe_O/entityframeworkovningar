﻿using EfSamurai.Data;
using EfSamurai.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;

namespace EfSamurai.App
{
    public class Program
    {
        static void Main(string[] args)
        {
            AddOneSamuraiWithRelatedData();
            AddSomeSamurais();
            AddSomeSamurais();
            AddOneSamurai();

            Console.WriteLine("-----------------------");

            ListAllQuotesOfType(QuoteTypes.Lame);

            Console.WriteLine("Press ENTER to continue...");
            Console.ReadLine();
            ClearDatabase();
        }

        private static void AddOneSamurai()
        {
            var samuari = new Samurai { Name = "Zelda" };

            using (var context = new SamuraiContext())
            {
                context.Samurais.Add(samuari);
                context.SaveChanges();
            }
        }

        private static void AddSomeSamurais()
        {
            List<Samurai> samuraiList = new List<Samurai>();
            samuraiList.Add(new Samurai { Name = "TheUgly", Weapon = "HisBreath", HairStyle = HairStyles.Chonmage});
            samuraiList.Add(new Samurai { Name = "TheUgl", Weapon = "HisBreat", Quotes = new List<Quote>
            {
                new Quote
                {
                    QuoteType = QuoteTypes.Lame, Text = "Some very lame quote, so original!"
                }
            }
            });
            samuraiList.Add(new Samurai { Name = "TheUg", Weapon = "HisBrea" });

            using (var context = new SamuraiContext())
            {
                context.Samurais.AddRange(samuraiList);
                context.SaveChanges();
            }
        }

        private static void AddSomeBattles()
        {
            List<Battle> battles = new List<Battle>();
            battles.Add(new Battle{ 
                Name = "Hastings",
                Description = "Some battle!",
                Brutal = false,
                StartTime = DateTime.Today,
                EndTime = DateTime.MaxValue,
                BattleLog = new BattleLog
                {
                    BattleEvents = new List<BattleEvent>
                    {
                        new BattleEvent
                        {
                            Description = "Some battle on the fields", Summation = "It ended well"
                        },
                        new BattleEvent
                        {
                            Description = "Some battle on the plains", Summation = "It ended badly"
                        }
                    },
                    Name = "Hastings Log"
                }});
            battles.Add(new Battle
            {
                
                Name = "PlainField",
                Description = "Some battle on the ground with pikes",
                Brutal = true,
                StartTime = DateTime.Today,
                EndTime = DateTime.MaxValue,
                BattleLog = new BattleLog
                {
                    BattleEvents = new List<BattleEvent>
                    {
                        new BattleEvent
                        {
                            Description = "Some battle on the fields with pikes", Summation = "It ended well"
                        },
                        new BattleEvent
                        {
                            Description = "Some battle on the plains without pikes", Summation = "It ended badly"
                        }
                    },
                    Name = "PlainField Log"
                }
            });
            battles.Add(new Battle {
                Name = "Sudoku",
                Description = "Some battle with samurais that got the wrong idea",
                Brutal = true,
                StartTime = DateTime.Today,
                EndTime = DateTime.MaxValue,
                BattleLog = new BattleLog
                {
                    BattleEvents = new List<BattleEvent>
                    {
                        new BattleEvent
                        {
                            Description = "Some battle on the fields with suicidal samurais", Summation = "It ended well"
                        },
                        new BattleEvent
                        {
                            Description = "Some tea serving on the plains with samurais that had changed their minds", Summation = "It ended badly"
                        }
                    },
                    Name = "Sudoku Log"
                }
            });
            
            using (var context = new SamuraiContext())
            {
                context.Battles.AddRange(battles);
                context.SaveChanges();
            }
        }

        private static void AddOneSamuraiWithRelatedData()
        {
            var samurai = new Samurai
            {
                Name = "Herro",
                Armor = "Scales",
                HairStyle = HairStyles.Chonmage
            };
            var quotes = new List<Quote>()
            {
                new Quote
                {
                    QuoteType = QuoteTypes.Lame,
                    Text = "The new best samurai, is me",
                    Samurai = samurai
                },
                new Quote
                {
                    QuoteType = QuoteTypes.Awesome,
                    Text = "The new best samurai, is someone else",
                    Samurai = samurai
                }
            };
            var secretId = new SecretIdentity
            {
                RealName = "Danzo",
                Samurai = samurai
            };
            var battle = new Battle
            {
                Name = "Constantinople",
                Brutal = true,
                Description = "Some battle it was!",
                StartTime = DateTime.Today,
                EndTime = DateTime.Today.AddDays(10)
            };
            var battleLog = new BattleLog
            {
                Name = "Const log",
                Battle = battle
            };
            var samuraiBattles = new SamuraiBattle
            {
                Battle = battle,
                Samurai = samurai
            };
            var battleEvents = new List<BattleEvent>
            {
                new BattleEvent
                {
                    Description = "Some battle",
                    Summation = "It ended",
                    BattleLog = battleLog
                },
                new BattleEvent
                {
                    Description = "Battle wasn't done yet!",
                    Summation = "We got backstabbed",
                    BattleLog = battleLog
                }
            };

            using (var context = new SamuraiContext())
            {
                context.Samurais.Add(samurai);
                context.Quotes.AddRange(quotes);
                context.SecretIdentities.Add(secretId);
                context.Battles.Add(battle);
                context.BattleLogs.Add(battleLog);
                context.BattleEvents.AddRange(battleEvents);
                context.SamuraiBattles.Add(samuraiBattles);
                context.SaveChanges();
            }
        }

        private static void ClearDatabase()
        {
            using (var context = new SamuraiContext())
            {
                var battles = context.Battles.Select(b => b);
                context.Battles.RemoveRange(battles);

                var battleEvents = context.BattleEvents.Select(be => be);
                context.BattleEvents.RemoveRange(battleEvents);

                var battleLogs = context.BattleLogs.Select(bl => bl);
                context.BattleLogs.RemoveRange(battleLogs);

                var quotes = context.Quotes.Select(q => q);
                context.Quotes.RemoveRange(quotes);

                var secretIdentities = context.SecretIdentities.Select(si => si);
                context.SecretIdentities.RemoveRange(secretIdentities);

                var samuraiBattles = context.SamuraiBattles.Select(sb => sb);
                context.SamuraiBattles.RemoveRange(samuraiBattles);

                var samurais = context.Samurais.Select(s => s);
                context.Samurais.RemoveRange(samurais);

                context.SaveChanges();
            }
        }

        private static void ListAllSamuraiNames()
        {
            using (var context = new SamuraiContext())
            {
                foreach (Samurai samurai in context.Samurais.Local)
                {
                    Console.WriteLine(samurai.Name);
                }
            }
        }

        private static List<Samurai> ListallSamuraiNames_OrderByName()
        {
            using (var context = new SamuraiContext())
            {
                return context.Samurais.OrderBy(s => s.Name).ToList();
            }
        }

        private static List<Samurai> ListAllSamuraiNames_OrderByDescending()
        {
            using (var context = new SamuraiContext())
            {
                return context.Samurais.OrderByDescending(x => x.Name).ToList();
            }
        }

        private static bool FindSamuraiWithRealName(string name)
        {
            using (var context = new SamuraiContext())
            {
                if (context.Samurais.Any(x => x.SecretIdentity.RealName.ToLower() == name.ToLower()))
                {
                    Console.WriteLine("The samurai was found!");
                    return true;
                }

                Console.WriteLine("The samurai was not found!");
                return false;
            }
        }

        public static List<Quote> ListAllQuotesOfType(QuoteTypes quoteTypes)
        {
            using (var context = new SamuraiContext())
            {
                return context.Quotes.Where(x => x.QuoteType == quoteTypes).ToList();
            }
        }

        public static List<>
    }
}